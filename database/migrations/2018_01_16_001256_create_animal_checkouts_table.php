<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimalCheckoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animal_checkouts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('animal_id');
            $table->integer('user_id');
            $table->integer('visit_type_id');
            $table->integer('times_handled')->nullable();
            $table->dateTime('checked_out_at');
            $table->dateTime('checked_in_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animal_checkouts');
    }
}
