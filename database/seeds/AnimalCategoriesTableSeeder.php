<?php

use Illuminate\Database\Seeder;

class AnimalCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('animal_categories')->insert([
            'name' => 'Amphibians'
        ]);

        DB::table('animal_categories')->insert([
            'name' => 'Invertebrates'
        ]);

        DB::table('animal_categories')->insert([
            'name' => 'Reptiles'
        ]);

        DB::table('animal_categories')->insert([
            'name' => 'Birds'
        ]);

        DB::table('animal_categories')->insert([
            'name' => 'Mammals'
        ]);
    }
}
