<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Aquarium Admin',
            'email' => 'admin@thelivingplanet.com',
            'password' => bcrypt('uwrahE'),
            'role' => 'admin'
        ]);
    }
}
