<?php

use Illuminate\Database\Seeder;

class AnimalSpeciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('animal_species')->insert([
            'name' => 'Tiger Salamanders',
            'animal_category_id' => 1
        ]);

        DB::table('animal_species')->insert([
            'name' => 'White Treefrogs',
            'animal_category_id' => 1
        ]);

        DB::table('animal_species')->insert([
            'name' => 'Red-spotted Toads',
            'animal_category_id' => 1
        ]);

        DB::table('animal_species')->insert([
            'name' => "Woodhouse's Toads",
            'animal_category_id' => 1
        ]);

        DB::table('animal_species')->insert([
            'name' => 'Madagascar Hissing Cockroach',
            'animal_category_id' => 2
        ]);

        DB::table('animal_species')->insert([
            'name' => 'Giant Cave Cockroach',
            'animal_category_id' => 2
        ]);

        DB::table('animal_species')->insert([
            'name' => 'Chilean Rose Hair Tarantulas',
            'animal_category_id' => 2
        ]);

        DB::table('animal_species')->insert([
            'name' => 'Mexican Red Knee Tarantula',
            'animal_category_id' => 2
        ]);

        DB::table('animal_species')->insert([
            'name' => 'Hermit Crabs',
            'animal_category_id' => 2
        ]);

        DB::table('animal_species')->insert([
            'name' => 'New Caledonia Giant Gecko',
            'animal_category_id' => 3
        ]);

        DB::table('animal_species')->insert([
            'name' => 'Crested Geckos',
            'animal_category_id' => 3
        ]);

        DB::table('animal_species')->insert([
            'name' => 'Red-eyed Crocodile Skinks',
            'animal_category_id' => 3
        ]);

        DB::table('animal_species')->insert([
            'name' => 'Blue Tongue Skinks',
            'animal_category_id' => 3
        ]);

        DB::table('animal_species')->insert([
            'name' => 'Argentine Black and White Tegus',
            'animal_category_id' => 3
        ]);

        DB::table('animal_species')->insert([
            'name' => 'Russian Tortoises',
            'animal_category_id' => 3
        ]);

        DB::table('animal_species')->insert([
            'name' => 'Sonoran Mountain Kingsnakes',
            'animal_category_id' => 3
        ]);

        DB::table('animal_species')->insert([
            'name' => 'Wandering Garter Snake',
            'animal_category_id' => 3
        ]);

        DB::table('animal_species')->insert([
            'name' => 'Great Basin Gopher Snakes',
            'animal_category_id' => 3
        ]);

        DB::table('animal_species')->insert([
            'name' => 'Red-tail Boa Constrictors',
            'animal_category_id' => 3
        ]);

        DB::table('animal_species')->insert([
            'name' => "Bredli's Carpet Pythons",
            'animal_category_id' => 3
        ]);

        DB::table('animal_species')->insert([
            'name' => 'Rainbow Boa Constrictors',
            'animal_category_id' => 3
        ]);

        DB::table('animal_species')->insert([
            'name' => 'Borneo Short-tail Blood Pythons',
            'animal_category_id' => 3
        ]);

        DB::table('animal_species')->insert([
            'name' => 'African Ball Pythons',
            'animal_category_id' => 3
        ]);

        DB::table('animal_species')->insert([
            'name' => 'Eclectus Parrot',
            'animal_category_id' => 4
        ]);

        DB::table('animal_species')->insert([
            'name' => 'Congo African Grey Parrot',
            'animal_category_id' => 4
        ]);

        DB::table('animal_species')->insert([
            'name' => 'Yellow-headed Amazon Parrot',
            'animal_category_id' => 4
        ]);

        DB::table('animal_species')->insert([
            'name' => 'African Pygmy Hedgehogs',
            'animal_category_id' => 5
        ]);

        DB::table('animal_species')->insert([
            'name' => 'Southern Three Banded Armadillos',
            'animal_category_id' => 5
        ]);
    }
}
