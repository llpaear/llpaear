<?php

use Illuminate\Database\Seeder;

class AccessCodeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('access_codes')->insert([
            'code' => '1234'
        ]);
    }
}
