<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController@index')->name('dashboard');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('user', 'UserController');
Route::get('animals/checked-out', 'AnimalController@showCheckedOutAnimals');
Route::get('animals/{id}/checkout', 'AnimalController@showCheckoutPage');
Route::post('animals/{id}/checkout', 'AnimalController@checkoutAnimal')->name('animals.checkout');
Route::get('animals/{id}/check-in', 'AnimalController@showCheckinPage');
Route::post('animals/{id}/check-in', 'AnimalController@checkinAnimal')->name('animals.checkin');
Route::resource('animals', 'AnimalController');
Route::get('/animal/{id}', 'AnimalController@show');
Route::get('/animal/{id}/add-note', 'AnimalController@showAddAnimalNotePage');
Route::post('/animal/{id}/add-note', 'AnimalController@addAnimalNote')->name('animals.add-note');


Route::get('admin/settings', 'AdminController@showSettings');
Route::post('admin/settings/visit-type/add', 'AdminController@addVisitType')->name('admin.add_type');

Route::get('admin/visit-types', 'AdminController@showVisitTypes');

Route::get('admin/certifications', 'AdminController@showHandlingCertificationSettings');
Route::post('admin/certifications', 'AdminController@addHandlingCertifications')->name('admin.certifications');
Route::get('admin/employees', 'AdminController@showEmployeeList');
Route::post('admin/employee/{id}/renew', 'AdminController@renewCertification');
Route::delete('admin/employee/{id}', 'AdminController@deleteCertification');
Route::delete('admin/animal/{id}', 'AdminController@deleteAnimal');
Route::get('admin/employee/{id}', 'AdminController@showEmployee')->name('admin.employee');
Route::get('admin/access-code', 'AdminController@showAccessCodeForm');
Route::post('admin/access-code', 'AdminController@changeAccessCode')->name('admin.access-code');
Route::post('admin/employee/{id}/role', 'AdminController@changeEmployeeRole')->name('admin.employee-role');
Route::get('admin/categories', 'AdminController@showAnimalCategories');
Route::post('admin/categories', 'AdminController@createAnimalCategory')->name('admin.categories');
Route::patch('admin/categories/{id}', 'AdminController@editAnimalCategory');
Route::delete('admin/categories/{id}', 'AdminController@deleteAnimalCategory');
Route::get('admin/species', 'AdminController@showAnimalSpecies');
Route::post('admin/species', 'AdminController@createAnimalSpecie')->name('admin.species');
Route::patch('admin/species/{id}', 'AdminController@editAnimalSpecies');
Route::delete('admin/species/{id}', 'AdminController@deleteAnimalSpecies');