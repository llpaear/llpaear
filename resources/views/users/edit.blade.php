@extends('layouts.default')

@section('content')
<div class="ui segment">
    <div class="ui header">Edit Profile for {{$user->name}}</div>

    <div class="content">
        <form class="ui form" method="POST" action="{{ route('user.update', $user->id) }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}

            <div class="field">
                <label for="name">Name</label>
                <input id="name" type="text" name="name" value="{{$user->name}}" required autofocus>
            </div>

            <div class="field">
                <label for="email">E-Mail Address</label>
                <input id="email" type="email" name="email" value="{{$user->email}}" required>
            </div>

            <div class="field">
                <label for="phone_number">Telephone Number</label>
                <input id="phone_number" type="tel" name="phone_number" value="{{$user->phone_number}}">
            </div>

            <div class="field">
                <label for="image">Profile Image</label>
                <input id="image" type="file" name="image">
            </div>

            <button type="submit" class="ui orange button">Update Profile</button>
        </form>
    </div>
</div>
@endsection