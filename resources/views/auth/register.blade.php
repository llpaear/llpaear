@extends('layouts.app')

@section('content')
    @if ($errors->first('access_code'))
    <div class="ui negative message">
        <div class="header">Access Code Error</div>
        <p>That access code is incorrect. Please verify you are using the correct access code.</p>
    </div>
    @endif
    <div class="ui segment">
        <div class="ui header">Register</div>

        <div class="content">
            <form class="ui form" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}

                <div class="field">
                    <label for="name">Name</label>
                    <input id="name" type="text" name="name" value="{{ old('name') }}" required autofocus>
                </div>

                <div class="field">
                    <label for="email">E-Mail Address</label>
                    <input id="email" type="email" name="email" value="{{ old('email') }}" required>
                </div>

                <div class="field">
                    <label for="password">Password</label>
                    <input id="password" type="password" name="password" required>
                </div>

                <div class="field">
                    <label for="password-confirm">Confirm Password</label>
                    <input id="password-confirm" type="password" name="password_confirmation" required>
                </div>

                <div class="field">
                    <label for="access_code">Access Code</label>
                    <input id="access_code" type="password" name="access_code" required>
                </div>

                <button type="submit" class="ui orange button">Register</button>
            </form>
        </div>
    </div>
@endsection
