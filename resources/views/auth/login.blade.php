@extends('layouts.app')

@section('content')
<div style="position:relative;bottom:100px;">
    <div class="ui center aligned basic segment">
        <img class="ui centered medium image"  src="{{asset('img/llpa-logo.png')}}">
        <h1 style="margin:0;">EAR Tracking</h1>
    </div>
    <div class="ui segment">
        <div class="ui header">Login</div>

        <div class="content">
            <form class="ui form" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                <div class="field">
                    <label for="email">E-Mail Address</label>
                    <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus>
                </div>

                <div class="field">
                    <label for="password">Password</label>
                    <input id="password" type="password" name="password" required>
                </div>

                <div class="field">
                    <div class="ui checkbox">
                        <label>Remember Me</label>
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                    </div>
                </div>

                <button type="submit" class="ui orange button">Login</button>
                <a href="{{ route('password.request') }}">Forgot Your Password?</a>
            </form>
            <br><br>
            <a href="{{url('/register')}}">Don't have an account yet? Register Here</a>
        </div>
    </div>
</div>
@endsection
