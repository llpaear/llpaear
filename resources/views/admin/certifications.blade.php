@extends('layouts.default')

@section('content')
    <div class="ui segment">
        <h2 class="ui horizontal divider header">
            Assign Certifications
        </h2>
        <div class="content">
            <form class="ui form" method="POST" action="{{ route('admin.certifications') }}" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="field">
                    <label for="user_id">User</label>
                    <select class="ui dropdown" id="user_id" name="user_id" required>
                        @foreach ($users as $user)
                        <option value="{{$user->id}}">{{$user->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="field">
                    <label for="cert-length">Length of Certifications (Days until expiration)</label>
                    <input id="cert-length" type="number" name="cert-length" requireds>
                </div>

                <div class="field">
                    <label>Add Certifications for these Animal Species</label>
                    @foreach ($animal_species as $animal_specie)
                    <div class="ui checkbox tile">
                        <input name="animal_species_{{$animal_specie->id}}" id="{{$animal_specie->id}}" type="checkbox" tabindex="0" class="hidden" value="{{$animal_specie->id}}">
                        <label>{{$animal_specie->name}}</label>
                    </div>
                    @endforeach
                </div>

                <button type="submit" class="ui orange button">Add Certifications</button>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
@parent
<script>
    $('.ui.checkbox').checkbox();
</script>
@endsection