@extends('layouts.default')

@section('content')
<div class="content">
    <div class="segment ui">
            <h2 class="ui horizontal divider header">
                Manage Employees
            </h2>

            <div class="ui middle aligned divided list">
                @foreach ($users as $user)
                    <div class="item">
                      <img class="ui avatar image" src="{{$user->image_url}}">
                      <div class="content">
                        <a class="header" href="{{route('admin.employee', ['id' => $user->id])}}">{{$user->name}}</a>
                      </div>
                    </div>
                @endforeach
            </div>

    </div>
</div>
@endsection