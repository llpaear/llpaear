@extends('layouts.default')

@section('content')
    <div class="ui segment">
        <h2 class="ui horizontal divider header">
            Manage Animal Categories
        </h2>
        <div class="content">
            <form class="ui form" method="POST" action="{{ route('admin.categories') }}" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="field">
                    <label for="category">Animal Category Name</label>
                    <input id="category" type="text" name="category" required autofocus>
                </div>

                <button type="submit" class="ui orange button">Add Category</button>
            </form>
            <br>
            <h4>Categories</h4>
            <item-list :list-data="{{$animal_categories}}" :type="'categories'"></item-list>
        </div>
    </div>
@endsection