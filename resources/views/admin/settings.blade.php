@extends('layouts.default')

@section('content')
    <div class="ui segment admin-settings">
        <h2 class="ui horizontal divider header">
                Admin Settings
            </h2>
        <div class="ui cards flex-center">
            <a class="item card tile hover" href="{{url('admin/certifications')}}">
                <div class="content">
                    <div class="ui horizontal divider"></div>
                    <div class="header">
                        Add Certifications
                    </div>
                </div>
            </a>
            <a class="item card tile hover" href="{{url('admin/visit-types')}}">
                <div class="content">
                    <div class="ui horizontal divider"></div>
                    <div class="header">
                        Add Visit Types
                    </div>
                </div>
            </a>
            <a class="item card tile hover" href="{{url('admin/employees')}}">
                <div class="content">
                    <div class="ui horizontal divider"></div>
                    <div class="header">
                        Edit Employees
                    </div>
                </div>
            </a>
            <a class="item card tile hover" href="{{url('admin/access-code')}}">
                <div class="content">
                    <div class="ui horizontal divider"></div>
                    <div class="header">
                        Change Access Code
                    </div>
                </div>
            </a>
            <a class="item card tile hover" href="{{url('admin/categories')}}">
                <div class="content">
                    <div class="ui horizontal divider"></div>
                    <div class="header">
                        Manage Animal Categories
                    </div>
                </div>
            </a>
            <a class="item card tile hover" href="{{url('admin/species')}}">
                <div class="content">
                    <div class="ui horizontal divider"></div>
                    <div class="header">
                        Manage Animal Species
                    </div>
                </div>
            </a>
            <a class="item card tile hover" href="{{url('animals/create')}}">
                <div class="content">
                    <div class="ui horizontal divider"></div>
                    <div class="header">
                        Add Animal
                    </div>
                </div>
            </a>
        </div>
    </div>
@endsection