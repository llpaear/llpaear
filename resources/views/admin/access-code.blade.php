@extends('layouts.default')

@section('content')
    <div class="ui segment">
        <div class="content">
            <h2 class="ui horizontal divider header">
                Current Access Code: {{$access_code->code}}
            </h2>
            <form class="ui form" method="POST" action="{{ route('admin.access-code') }}" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="field">
                    <label for="access-code">Access Code</label>
                    <input id="access-code" type="text" name="access-code" required autofocus>
                </div>

                <button type="submit" class="ui orange button">Change Access Code</button>
            </form>
        </div>
    </div>
@endsection