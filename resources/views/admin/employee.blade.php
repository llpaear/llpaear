@extends('layouts.default')

@section('content')
<div class="content ui segment">
    <h3>{{$user->name}}</h3>
    <p><h4>Current Role: {{$user->role}}</h4></p>
    <form class="ui form" method="POST" action="{{ route('admin.employee-role', ['id' => $user->id]) }}" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="field">
            <label for="user_role">User Role</label>
            <select class="ui dropdown" id="user_role" name="user_role" required>
                <option value="employee">Employee</option>
                <option value="admin">Admin</option>
            </select>
        </div>

        <button type="submit" class="ui orange button">Change Role</button>
    </form>
    <br><br>
    <certification-renewal :handling-certifications="{{$user->handling_certifications}}" :user="{{$user}}"></certification-renewal>
</div>
@endsection