@extends('layouts.default')

@section('content')
    <div class="ui segment">
        <h2 class="ui horizontal divider header">
            Manage Animal Species
        </h2>
        <div class="content">
            <form class="ui form" method="POST" action="{{ route('admin.species') }}" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="field">
                    <label for="animal_category_id">Animal Category</label>
                    <select class="ui dropdown" id="animal_category_id" name="animal_category_id" required>
                        @foreach ($animal_categories as $animal_category)
                        <option value="{{$animal_category->id}}">{{$animal_category->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="field">
                    <label for="species">Animal Species Name</label>
                    <input id="species" type="text" name="species" required autofocus>
                </div>

                <button type="submit" class="ui orange button">Add Species</button>
            </form>
            <br>
            <h4>Species</h4>
            <item-list :list-data="{{$animal_species}}" :type="'species'"></item-list>
        </div>
    </div>
@endsection