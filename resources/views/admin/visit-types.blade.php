@extends('layouts.default')

@section('content')
<div class="content">
    <div class="segment ui">
        <h2 class="ui horizontal divider header">
            Manage Visit Types
        </h2>
        <form class="ui form" method="POST" action="{{ route('admin.add_type') }}" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="field">
                <label for="title">Title</label>
                <input id="title" type="text" name="title" value="{{ old('title') }}" required autofocus>
            </div>

            <div class="field">
                <label for="description">Description</label>
                <textarea id="description" name="description" value="{{ old('description') }}" required autofocus></textarea>
            </div>

            <button type="submit" class="ui orange button">Add Visit Type</button>
        </form>
    </div>
</div>
@endsection