<div>
    <?php
        $url = url('/') . '/';
    ?>
    <div class="ui right sidebar vertical menu side-menu-mobile">
        <div class="item">
            <img src="{{asset('img/llpa-logo.png')}}">
        </div>
        <a class="item" href="{{url('/')}}">Dashboard</a>
        <a class="item" href="{{url('animals')}}">Avaliable Animals</a>
        <a class="item" href="{{url('animals/checked-out')}}">Checked Out Animals</a>
        @if (Auth::user()->role == 'admin')
        <a class="item"  href="{{url('admin/settings')}}">Admin</a>
        @endif
        <a class="item" href="{{url('user', [Auth::user()->id])}}">
            @if($user->image_url !== $url)
                <img class="ui avatar image" src="{{$user->image_url}}" alt="">
            @else
                <i style="float:none;margin:0;" class="user icon large"></i>
            @endif
            <span>{{$user->name}}</span>
        </a>
        <a class="item" href="/logout"
            onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
            Logout
        </a>
    </div>
    <div class="ui container">
        <div class="ui top secondary menu fixed mobile-menu">
            <div class="ui dropdown icon item">
                <a href="{{ url()->previous() }}">
                    <i class="chevron left icon big"></i>
                </a>
            </div>
            <div class="right menu">
                <a class="toc item" onclick="openSidebar()">
                    <i class="sidebar icon big"></i>
                </a>
            </div>
        </div>

        <div style="margin-bottom:10px;" class="ui fixed menu desktop-only desktop-menu">
            <a class="item" href="{{url('/')}}">
                Dashboard
            </a>
            <a class="item" href="{{url('animals')}}">
                Avaliable Animals
            </a>
            <a class="item" href="{{url('animals/checked-out')}}">
                Checked Out Animals
            </a>
            @if (Auth::user()->role == 'admin')
            <a class="item"  href="{{url('admin/settings')}}">Admin</a>
            @endif
            <div class="right menu">
                <a class="item" href="{{url('user', [Auth::user()->id])}}">
                        <div class="ui fluid category search">
                            <div class="content">
                            @if($user->image_url !== $url)
                                <img class="ui avatar image" src="{{$user->image_url}}" alt="">
                            @else
                                <i class="user icon"></i>
                            @endif
                                <span>{{$user->name}}</span>
                            </div>
                        </div>
                    </a>
                <a class="ui item" href="/logout"
                    onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                    Logout
                </a>
                <form id="logout-form" action="/logout" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
</div>

@section('scripts')
<script>
    openSidebar = function() {
        window.$('.ui.sidebar').sidebar('toggle');
    }
</script>
@endsection