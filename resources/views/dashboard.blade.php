@extends('layouts.default')

@section('content')
    <dashboard :user="{{$user}}" :animals="{{$animals}}" :categories="{{$categories}}" :animal-checkouts="{{$animal_checkouts}}"></dashboard>
@endsection