@extends('layouts.default')

@section('content')
    <checked-out :animals="{{$animal_checkouts}}" :history="false"></checked-out>
    <br><br><br>
    <checked-out :animals="{{$all_animal_checkouts}}" :history="true"></checked-out>
@endsection