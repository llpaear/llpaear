@extends('layouts.default')
@section('content')
    <search 
    :animals="{{$animals}}" 
    :categories="{{$categories}}" 
    :species="{{$species}}"
    :user="{{Auth::user()}}"></search>
@endsection