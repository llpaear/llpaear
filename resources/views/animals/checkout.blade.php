@extends('layouts.default')

@section('content')
    <div class="ui segment">
    <div class="ui header">Checkout {{$animal->name}}</div>

    <div class="content">
        <form class="ui form" method="POST" action="{{ route('animals.checkout', $animal->id) }}" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="field">
                <label for="checked_out_by">Check out by</label>
                <input type="text" class="ui dropdown" disabled value="{{$user->name}}">
                <input type="hidden" id="checked_out_by" name="checked_out_by" value="{{$user->id}}">
            </div>

            <div class="field">
                <label for="visit_purpose">Purpose of Visit</label>
                <select class="ui dropdown" id="visit_purpose" name="visit_purpose" required>
                    @foreach ($visit_types as $visit_type)
                    <option value="{{$visit_type->id}}">{{$visit_type->title}}</option>
                    @endforeach
                </select>
            </div>

            <div class="field">
                <label for="checkout_time">Time of Checkout</label>
                <div class="ui input left icon">
                    <i class="time icon"></i>
                    <input id="checkout_time" type="text" name="checkout_time" value="{{$time}}" disabled>
                </div>
            </div>

            @if ($can_check_out)
                <button style="display:block;" type="submit" class="ui orange button">Checkout Animal</button>
            @else
                <div style="display:block;" class="ui disabled orange button">Checkout Animal</div>
                <div class="ui pointing label">
                    Animal was checked out <br> <br> or was handled too many times.
                </div>
            @endif
        </form>
    </div>
</div>
@endsection

@section('scripts')
@parent
<script>
    window.$('.ui.dropdown').dropdown();
</script>
@endsection