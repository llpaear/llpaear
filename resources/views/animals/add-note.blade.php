@extends('layouts.default')

@section('content')
<div class="segment ui">
    <h2 class="ui horizontal divider header">
        Add a  Note
    </h2>
    <div class="content">
        <form class="ui form" method="POST" action="{{ route('animals.add-note', $animal->id) }}" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="field">
                <label for="note">Note</label>
                <textarea id="note" name="note"></textarea>
            </div>

            <button type="submit" class="ui orange button">Add Note</button>
        </form>
    </div>
</div>
@endsection

@section('scripts')
@parent
<script>
    window.$('.ui.dropdown').dropdown();
</script>
@endsection