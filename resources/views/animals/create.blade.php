@extends('layouts.default')

@section('content')

<div class="ui segment">
    <div class="ui header">Add Animal</div>
    <div class="content">
        <form class="ui form" method="POST" action="{{ route('animals.store') }}" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="field">
                <label for="name">Name</label>
                <input id="name" type="text" name="name" required autofocus>
            </div>

            <div class="field">
                <label for="nickname">Nickname</label>
                <input id="nickname" type="text" name="nickname" required>
            </div>

            <div class="field">
                    <label for="species">Species</label>
                    <select class="ui dropdown" id="species" name="species" required>
                        @foreach($species as $specie)
                            <option value="{{$specie->id}}">{{$specie->name}}</option>
                        @endforeach
                    </select>
                </div>

            <div class="field">
                <label for="image">Image</label>
                <input id="image" type="file" name="image" required>
            </div>

            <div class="field">
                <label for="bio">Animal Bio</label>
                <textarea id="bio" name="bio"></textarea>
            </div>

            <button type="submit" class="ui orange button">Add Animal</button>
        </form>
    </div>
</div>
@endsection

@section('scripts')
@parent
<script>
    window.$('.ui.dropdown').dropdown();
</script>
@endsection