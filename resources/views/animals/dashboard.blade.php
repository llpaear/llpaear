@extends('layouts.default')

@section('content')
    <animal-dashboard :animal="{{$animal}}" :animal-notes="{{$animalNotes}}"></animal-dashboard>
@endsection 