@extends('layouts.default')

@section('content')
    <div class="content">
        <form class="ui form" method="POST" action="{{ route('animals.checkin', $animal_checkout->id) }}" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="field">
                <label>Animal</label>
                <p>{{$animal->name}} ({{$animal->nickname}})</p>
            </div>

            <div class="field">
                <label>Checked Out By</label>
                <p>{{$checked_out_user->name}}</p>
            </div>

            <div class="field">
                <label>Checked Out</label>
                <p>{{$animal_checkout->checked_out_at_readable}}</p>
            </div>

            <div class="field">
                <label for="checked_out_by">Check in by</label>
                <input type="text" class="ui dropdown" disabled value="{{$user->name}}">
                <input type="hidden" id="checked_in_by" name="checked_in_by" value="{{$user->id}}">
            </div>

            <div class="field">
                <label for="checkin_time">Time of Check-in</label>
                <div class="ui input left icon">
                    <i class="time icon"></i>
                    <input id="checkin_time" type="text" name="checkin_time" value="{{$time}}" disabled>
                </div>
            </div>

            <div class="field">
                <label for="times_handled">Times Handled</label>
                <div class="ui input">
                    <input id="times_handled" type="number" name="times_handled">
                </div>
            </div>

            <div class="field">
                <label for="note">Note</label>
                <textarea id="note" name="note"></textarea>
            </div>

            <button type="submit" class="ui orange button">Check In {{$animal->name}}</button>
        </form>
    </div>
@endsection

@section('scripts')
@parent
<script>
    window.$('.ui.dropdown').dropdown();
</script>
@endsection