@extends('layouts.default')

@section('content')

<div class="ui segment">
    <div class="ui header">Add Animal</div>
    <div class="content">
        <form class="ui form" method="POST" action="{{ route('animals.update', ['id' => $animal->id]) }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            {!! method_field('patch') !!}

            <div class="field">
                <label for="name">Name</label>
                <input id="name" type="text" name="name" value="{{$animal->name}}" required autofocus>
            </div>

            <div class="field">
                <label for="nickname">Nickname</label>
                <input id="nickname" type="text" name="nickname" value="{{$animal->nickname}}" required>
            </div>

            <div class="field">
                    <label for="animal_species_id">Species</label>
                    <select class="ui dropdown" id="species" name="species" required>
                        @foreach($species as $specie)
                        @if ($specie->id == $animal->animal_species_id)
                            <option value="{{$specie->id}}" selected>{{$specie->name}}</option>
                        @else
                            <option value="{{$specie->id}}">{{$specie->name}}</option>
                        @endif
                        @endforeach
                    </select>
                </div>

            <img src="{{$animal->image_url}}" />
            <div class="field">
                <label for="image">Image</label>
                <input id="image" type="file" name="image" value="{{$animal->image}}">
            </div>

            <div class="field">
                <label for="bio">Animal Bio</label>
                <textarea id="bio" name="bio">{{$animal->bio}}</textarea>
            </div>

            <button type="submit" class="ui orange button">Update Animal</button>
        </form>
    </div>
</div>
@endsection

@section('scripts')
@parent
<script>
    window.$('.ui.dropdown').dropdown();
</script>
@endsection