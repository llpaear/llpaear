
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('dashboard' , require('./components/Dashboard.vue'));
Vue.component('search', require('./components/Search.vue'));
Vue.component('user-profile', require('./components/UserProfile.vue'));
Vue.component('list-animal', require('./components/ListAnimal.vue'));
Vue.component('animal-dashboard', require('./components/AnimalDashboard.vue'));
Vue.component('category-tile', require('./components/Tiles/CategoryTile.vue'));
Vue.component('species-tile', require('./components/Tiles/SpeciesTile.vue'));
Vue.component('animal-tile', require('./components/Tiles/AnimalTile.vue'));
Vue.component('handling-certification-list', require('./components/HandlingCertificationList.vue'));
Vue.component('checked-out', require('./components/CheckedOut.vue'));
Vue.component('checked-out-animal', require('./components/CheckedOutAnimal.vue'));
Vue.component('checked-out-history', require('./components/CheckedOutHistory.vue'));
Vue.component('certification-renewal', require('./components/CertificationRenewal.vue'));
Vue.component('item-list', require('./components/ItemList.vue'));

const app = new Vue({
    el: '#app'
});
