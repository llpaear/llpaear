<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitType extends Model
{
    protected $fillable = ['title', 'description'];
}
