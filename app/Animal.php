<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage;
use App\AnimalSpecie;

class Animal extends Model
{
    protected $fillable = ['name', 'nickname', 'animal_species_id', 'flagged', 'image', 'bio'];

    protected $hidden = ['image'];

    protected $appends = ['image_url', 'animal_specie'];

    public function getImageUrlAttribute()
    {
        $filesystem_driver = env('FILESYSTEM_DRIVER', 'local');
        if ($filesystem_driver === 'ftp') {
            return env('SERVER_URL').'/'.str_replace('storage/', 'public/', $this->image);
        }
        else {
            return asset($this->image);
        }
    }

    public function getAnimalSpecieAttribute()
    {
        return AnimalSpecie::find($this->animal_species_id);
    }
}
