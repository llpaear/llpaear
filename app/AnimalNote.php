<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnimalNote extends Model
{
    protected $fillable = ['animal_id', 'note', 'created_at'];
}
