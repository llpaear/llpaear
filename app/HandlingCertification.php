<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\AnimalSpecie;
use Carbon\Carbon;

class HandlingCertification extends Model
{
    protected $fillable = ['user_id', 'animal_species_id', 'expires'];

    protected $appends = ['animal_species', 'days_until_renewal'];

    public function getAnimalSpeciesAttribute()
    {
        return AnimalSpecie::find($this->animal_species_id);
    }

    public function getDaysUntilRenewalAttribute()
    {
        $today = new Carbon('today midnight');
        return $today->diffInDays(Carbon::parse($this->expires));
    }
}
