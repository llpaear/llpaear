<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Animal;
use App\AnimalNote;
use App\User;
use App\AnimalCheckout;
use App\AnimalCategory;
use App\AnimalSpecie;
use App\VisitType;
use Carbon\Carbon;
use Auth;

class AnimalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $animal_checkouts = AnimalCheckout::where('checked_in_at', null)->get()->toArray();
        $categories = AnimalCategory::all();
        $species = AnimalSpecie::all();
        $animals = Animal::all()->toArray();

        foreach ($animal_checkouts as $animal_checkout) {
            foreach ($animals as $index => $animal) {
                if ($animal['id'] == $animal_checkout['animal_id']) {
                    $animals[$index]['disabled'] = true;
                }
            }
        }

        $data = [
            'categories' => json_encode($categories),
            'animals' => json_encode($animals),
            'species' => $species
        ];

        return view('animals.list')->with($data);
    }

    public function showCheckedOutAnimals()
    {
        $animal_checkouts = AnimalCheckout::where('checked_in_at', null)->get();
        $all_animal_checkouts = AnimalCheckout::orderBy('checked_in_at', 'desc')->get();

        $data = [
            'animal_checkouts' => $animal_checkouts,
            'all_animal_checkouts' => $all_animal_checkouts
        ];

        return view('animals.checkedout')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = AnimalCategory::all();
        $species = AnimalSpecie::all();

        $data = [
            'categories' => $categories,
            'species' => $species
        ];

        return view('animals.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image_path = $request->file('image')->store('public');
        $image_path = str_replace('public/', 'storage/', $image_path);

        $animal = Animal::create([
            'name' => $request->input('name'),
            'nickname' => $request->input('nickname'),
            'animal_species_id' => $request->input('species'),
            'flagged' => false,
            'image' => $image_path,
            'bio' => $request->input('bio')
        ]);

        return redirect('animals');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $animal = Animal::find($id);
        $animalNotes = AnimalNote::where('animal_id',$id)
                                ->orderBy('created_at', 'desc')
                                ->get();
        return view('animals.dashboard')->with(
            ['animal' => $animal])->with(
            ['animalNotes' => $animalNotes]);
    }

    public function showCheckoutPage($id)
    {
        $animal = Animal::find($id);
        $animal_checkouts = AnimalCheckout::where('animal_id', $id)->where('checked_in_at', '>=', Carbon::yesterday())->get();
        $user = Auth::user();
        $visit_types = VisitType::all();
        $time = Carbon::now('America/Denver');
        $can_check_out = $this->canCheckOut($animal_checkouts);

        $data = [
            'animal' => $animal,
            'can_check_out' => $can_check_out,
            'user' => $user,
            'visit_types' => $visit_types,
            'time' => $time
        ];

        return view('animals.checkout')->with($data);
    }

    public function checkoutAnimal(Request $request, $id)
    {
        $animal = Animal::find($id);
        $user_id = $request->input('checked_out_by');
        $visit_type_id = $request->input('visit_purpose');
        $time = Carbon::now('America/Denver');

        $animal_checkout = AnimalCheckout::create([
            'animal_id' => $animal->id,
            'user_id' => $user_id,
            'visit_type_id' => $visit_type_id,
            'checked_out_at' => $time
        ]);

        return redirect('animals/checked-out');
    }

    public function showCheckinPage(Request $request, $id)
    {
        $animal_checkout = AnimalCheckout::find($id);
        $checked_out_user = User::find($animal_checkout->user_id);
        $animal = Animal::find($animal_checkout->animal_id);
        $user = Auth::user();
        $time = Carbon::now('America/Denver');

        $data = [
            'animal_checkout' => $animal_checkout,
            'animal' => $animal,
            'user' => $user,
            'time' => $time,
            'checked_out_user' => $checked_out_user
        ];

        return view('animals.checkin')->with($data);
    }

    public function checkinAnimal(Request $request, $id)
    {
        $animal_checkout = AnimalCheckout::find($id);
        $note = $request->input('note');
        $times_handled = $request->input('times_handled');
        $time = Carbon::now('America/Denver');

        if (!is_null($note)) {
            $animal_note = AnimalNote::create([
                'animal_id' => $animal_checkout->animal_id,
                'note' => $note
            ]);
        }

        $animal_checkout->update([
            'checked_in_at' => $time,
            'times_handled' => $times_handled
        ]);

        return redirect('animals');
    }

    public function showAddAnimalNotePage($id) {
        $animal = Animal::find($id);

        $data = [
            'animal' => $animal,
        ];

        return view('animals.add-note')->with($data);
    }

    public function addAnimalNote(Request $request, $id) {
        $note = $request->input('note');

        if (!is_null($note)) {
            $animal_note = AnimalNote::create([
                'animal_id' => $id,
                'note' => $note
            ]);
        }
        return redirect('animal/'.$id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $animal = Animal::find($id);
        $categories = AnimalCategory::all();
        $species = AnimalSpecie::all();

        $data = [
            'animal' => $animal,
            'categories' => $categories,
            'species' => $species
        ];

        return view('animals.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $animal = Animal::find($id);
        if (!is_null($request->file('image'))) {
            $image_path = $request->file('image')->store('public');
            $image_path = str_replace('public/', 'storage/', $image_path);
            $animal->update([
                'image' => $image_path
            ]);
        }

        $animal->update([
            'name' => $request->input('name'),
            'nickname' => $request->input('nickname'),
            'animal_species_id' => $request->input('species'),
            'flagged' => false,
            'bio' => $request->input('bio')
        ]);

        return redirect('animal/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function canCheckOut($animal_checkouts){
        $can_check_out = true;

        $times_handled = 0;
        if (count($animal_checkouts) > 0) {
            $checked_out_yesterday = Carbon::parse($animal_checkouts->last()->checked_in_at)->isYesterday();
            foreach ($animal_checkouts as $animal_checkout) {
                $times_handled += $animal_checkout->times_handled;
            }

            if ($checked_out_yesterday || $times_handled >= 5) {
                $can_check_out = false;
            }
        }

        return $can_check_out;
    }
}
