<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VisitType;
use App\AnimalSpecie;
use App\Animal;
use App\User;
use App\HandlingCertification;
use App\AccessCode;
use App\AnimalCategory;

use Carbon\Carbon;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function showSettings()
    {
        return view('admin.settings');
    }

    public function showVisittypes()
    {
        return view('admin.visit-types');
    }

    public function addVisitType(Request $request)
    {
        $visit_type = VisitType::create([
            'title' => $request->input('title'),
            'description' => $request->input('description')
        ]);

        return view('admin.settings');
    }

    public function showHandlingCertificationSettings()
    {
        $animal_species = AnimalSpecie::all();
        $users = User::all();

        $data = [
            'animal_species' => $animal_species,
            'users' => $users
        ];

        return view('admin.certifications')->with($data);
    }

    public function addHandlingCertifications(Request $request)
    {
        //dd($request->input());
        $user_id = $request->input('user_id');
        $input = $request->input();
        unset($input['user_id']);
        unset($input['_token']);
        $animal_ids = array();

        while (list($key, $value) = each($input)) {
            if (strpos($key, 'animal_species') !== false) {
                array_push($animal_ids, $value);
            }
        }
        
        $certLength = Carbon::now()->addDays($request->input('cert-length'));
        
        foreach ($animal_ids as $animal_species_id) {
            HandlingCertification::create([
                'user_id' => $user_id,
                'animal_species_id' => $animal_species_id,
                'expires' => $certLength
            ]);
        }
        return redirect('/admin/employee/' . $user_id);
    }

    public function showEmployeeList()
    {
        $users = User::all();

        $data = [
            'users' => $users
        ];

        return view('admin.employeelist')->with($data);
    }

    public function showEmployee($id)
    {
        $user = User::find($id);

        $data = [
            'user' => $user
        ];

        return view('admin.employee')->with($data);
    }

    public function renewCertification(Request $request, $id)
    {
        $length = $request->input('length');
        $ninety_days_from_now = Carbon::now()->addDays($length);
        $certification = HandlingCertification::find($id);
        $certification->update([
            'expires' => $ninety_days_from_now
        ]);

        return response(['certification' => $certification], 200);
    }

    public function deleteCertification($id)
    {
        $certification = HandlingCertification::find($id);
        $certification->delete();

        return response('Certification deleted', 204);
    }

    public function deleteAnimal($id)
    {
        $animal = Animal::find($id);
        $animal->delete();

        return response('Animal deleted', 204);
    }

    public function showAccessCodeForm()
    {
        $access_code = AccessCode::first();

        return view('admin.access-code')->with(['access_code' => $access_code]);
    }

    public function changeAccessCode(Request $request)
    {
        $new_code = $request->input('access-code');

        $access_code = AccessCode::first();

        $access_code->update([
            'code' => $new_code
        ]);

        return redirect('/admin/settings');
    }

    public function changeEmployeeRole(Request $request, $id)
    {
        $user = User::find($id);
        $role = $request->input('user_role');
        $user->update([
            'role' => $role
        ]);

        return redirect('/admin/employee/'.$id);
    }

    public function showAnimalCategories()
    {
        $animal_categories = AnimalCategory::all();

        return view('admin.categories')->with(['animal_categories' => $animal_categories]);
    }

    public function showAnimalSpecies()
    {
        $animal_categories = AnimalCategory::all();
        $animal_species = AnimalSpecie::all();

        return view('admin.species')->with(['animal_species' => $animal_species, 'animal_categories' => $animal_categories]);
    }

    public function createAnimalCategory(Request $request)
    {
        $name = $request->input('category');

        $animal_category = AnimalCategory::create([
            'name' => $name
        ]);

        return redirect('admin/categories');
    }

    public function editAnimalCategory(Request $request, $id)
    {
        $animal_category = AnimalCategory::find($id);
        $animal_category->update([
            'name' => $request->input('name')
        ]);

        return response($animal_category, 200);
    }

    public function deleteAnimalCategory($id)
    {
        $animal_category = AnimalCategory::find($id);
        $animal_category->delete();

        return response('Animal Category deleted', 204);
    }

    public function createAnimalSpecie(Request $request)
    {
        $name = $request->input('species');
        $animal_category_id = $request->input('animal_category_id');

        $animal_species = AnimalSpecie::create([
            'name' => $name,
            'animal_category_id' => $animal_category_id
        ]);

        return redirect('admin/species');
    }

    public function editAnimalSpecies(Request $request, $id)
    {
        $animal_species = AnimalSpecie::find($id);
        $animal_species->update([
            'name' => $request->input('name'),
        ]);

        return response($animal_species, 200);
    }

    public function deleteAnimalSpecies($id)
    {
        $animal_species = AnimalSpecie::find($id);
        $animal_species->delete();

        return response('Animal Species deleted', 204);
    }
}
