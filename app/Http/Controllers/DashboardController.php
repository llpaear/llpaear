<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Animal;
use App\AnimalCategory;
use App\AnimalCheckout;
use Auth;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();
        $animals = Animal::all();
        $categories = AnimalCategory::all();
        $animal_checkouts = AnimalCheckout::where('checked_in_at', null)->where('user_id', $user->id)->get()->toArray();

        $data = [
            'categories' => $categories,
            'user' => $user, 
            'animals' => $animals,
            'animal_checkouts' => json_encode($animal_checkouts)
        ];

        return view('dashboard')->with($data);
    }
}
