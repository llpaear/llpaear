<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Animal;
use App\VisitType;
use Carbon\Carbon;
use App\User;

class AnimalCheckout extends Model
{
    protected $fillable = ['animal_id', 'user_id', 'visit_type_id', 'times_handled', 'checked_out_at', 'checked_in_at'];

    protected $appends = ['animal', 'visit_type', 'checked_out_by', 'checked_out_at_readable', 'checked_in_at_readable'];

    public function getAnimalAttribute()
    {
        return Animal::find($this->animal_id);
    }

    public function getVisitTypeAttribute()
    {
        return VisitType::find($this->visit_type_id);
    }

    public function getCheckedOutByAttribute()
    {
        return User::find($this->user_id);
    }

    public function getCheckedOutAtReadableAttribute()
    {
        return Carbon::parse($this->checked_out_at)->toDayDateTimeString();
    }

    public function getCheckedInAtReadableAttribute()
    {
        if (!is_null($this->checked_in_at)) {
            return Carbon::parse($this->checked_in_at)->toDayDateTimeString();
        }
        else {
            return null;
        }
    }
}
