<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnimalSpecie extends Model
{
    protected $fillable = ['name', 'animal_category_id'];
}
