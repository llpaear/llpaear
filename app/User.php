<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Storage;
use App\HandlingCertification;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'image', 'phone_number'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['image_url', 'handling_certifications'];

    public function getImageUrlAttribute()
    {
        // this code is only so our demo environment supports images
        $filesystem_driver = env('FILESYSTEM_DRIVER', 'local');
        if ($filesystem_driver === 'ftp') {
            return env('SERVER_URL').'/'.str_replace('storage/', 'public/', $this->image);
        }
        else {
            return asset($this->image);
        }
    }

    public function getHandlingCertificationsAttribute()
    {
        return HandlingCertification::where('user_id', $this->id)->orderBy('expires', 'asc')->get();
    }
}
